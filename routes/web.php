<?php

use App\Http\Controllers\ContactController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ContactController::class, 'index'])->name('index');
Route::get('/create', [ContactController::class, 'create']);
Route::post('/store', [ContactController::class, 'store']);
Route::post('/update', [ContactController::class, 'update']);
Route::get('/edit/{id}', [ContactController::class, 'edit']);
Route::get('/destroy/{id}', [ContactController::class, 'destroy']);
