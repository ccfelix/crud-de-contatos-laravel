@extends('base')

@section('content')

    @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif

    @if (Session::has('fail'))
        <div class="alert alert-danger">
            {{ Session::get('fail') }}
        </div>
    @endif


    <div class="container">
        <div class="row mt-50">
            <div class="col-sm text-right p-30">
                <a href="create" class="btn btn-primary">Adicionar Novo Contato</a>
            </div>            
        </div>
        <div class="row">
            <div class="col-sm">
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Endereço</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($contacts as $c)
                            <tr>
                                <td>
                                    {{ $c->id }}
                                </td>
                                <td>
                                    {{ $c->name }}
                                </td>
                                <td>
                                    {{ $c->email }}
                                </td>
                                <td>
                                    {{ $c->address }}
                                </td>
                                <td class="text-right">
                                    <a class="btn btn-success" href="edit/{{ $c->id }}">Editar</a>
                                    <a class="btn btn-danger" href="destroy/{{ $c->id }}">Apagar</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2">
                                    Sem resultados
                                </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
