@extends('base')

@section('content')

    <form action="store" method="post" class="form">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $contact->id }}">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" name="name" value="{{ $contact->name }}" id="nome">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" name="email" value="{{ $contact->email }}" id="email">
        </div>
        <div class="form-group">
            <label for="email">Endereço</label>
            <input type="text" class="form-control" name="address" value="{{ $contact->address }}" id="address">
        </div>        
        <button type="submit" class="btn btn-primary">Salvar</button>
    </form>

@endsection
